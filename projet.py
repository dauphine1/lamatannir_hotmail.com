print("""
strings and loops
==================
""")

s = 'toto'

print("hello from gitlab editor")
=======

msg = f""" {s}
upper: {s.upper()}
xxx
"""

print(msg)

s = 'toto'
print (s)

============================ NEW COMMIT =================================

#Pierre Papier Ciseau

import random
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

print("\n*****---------******")
print("\nBonjour :)")

user = input("Vous vous appelez comment ? :) ")
print("\nEnchanté", user, "!",
      " \nCeci est le jeux du [Pierre / Feuille / Ciseau]\nSi vous voulez jouer avec LAMA :) veuillez "
      "suivre les instructions")

gains_Utilisateurs = 0
gains_LAMA  = 0
nbr_Total_Parties = 0
nbr_matchs_nuls = 0
x =[]
y =[]

options = ["pierre", "feuille", "ciseau"]

while True:
    utilisateurs_input = input("\nEcrire pierre/feuille/ciseau :) ou Q pour quitter :x ? ").lower()
    if utilisateurs_input == "q":
        print("\nVous ne voulez pas continuer à jouer avec LAMA :(\nok Pas de soucis, à une porchaine fois!")
        break

    if utilisateurs_input not in options:
        print("\nSérieux !! Cocentrer vous!")
        continue
    
    random_numéro = random.randint(0, 2)
    #Infos: pierre: 0, feuille: 1, ciseau: 2
    choix_LAMA = options[random_numéro]
    print("LAMA a choisit", choix_LAMA + ".")

    if utilisateurs_input == "pierre" and choix_LAMA == "ciseau":
        print("Vous avez gagné! :)")
        gains_Utilisateurs += 1
        nbr_Total_Parties += 1
        x.append(nbr_Total_Parties)
        y.append(gains_Utilisateurs)
    elif utilisateurs_input == "feuille" and choix_LAMA == "pierre":
        print("Vous avez gagné! :)")
        gains_Utilisateurs += 1
        nbr_Total_Parties += 1
        x.append(nbr_Total_Parties)
        y.append(gains_Utilisateurs)
    elif utilisateurs_input == "ciseau" and choix_LAMA == "feuille":
        print("Vous avez gagné! :)")
        gains_Utilisateurs += 1
        nbr_Total_Parties += 1
        x.append(nbr_Total_Parties)
        y.append(gains_Utilisateurs)
    elif utilisateurs_input ==  choix_LAMA:
        print("Egalité :D ")
        gains_Utilisateurs += 0
        nbr_Total_Parties += 1
        nbr_matchs_nuls += 1
        x.append(nbr_Total_Parties)
        y.append(gains_Utilisateurs)
    else:
        print("Vous avez perdu! :(")
        gains_LAMA  += 1
        nbr_Total_Parties += 1
        x.append(nbr_Total_Parties)
        y.append(gains_Utilisateurs)

if nbr_Total_Parties != 0:
    pourcentage_gain_Utilisateur = gains_Utilisateurs / nbr_Total_Parties * 100
else:
    pourcentage_gain_Utilisateur = 0

# Statistiques
print("\n-----Stats-----")
print("\n Vous avez remporté", gains_Utilisateurs, "matchs.")
print(" LAMA a remporté", gains_LAMA , "matchs.")
print(" Nombre matchs nuls : ", nbr_matchs_nuls)
print(" Pourcentage de vos gains : ", pourcentage_gain_Utilisateur, "%"  )
if gains_Utilisateurs > gains_LAMA:
    print("----  Bravo", user ,"Vous avez gagné !")
else:
    print("----  Lama a gagné :D")
print(" Au revoir!")

# Visualisation donnée
if nbr_Total_Parties > 0:
    yy = np.array([gains_Utilisateurs, gains_LAMA, nbr_matchs_nuls])
    Labels = [user, "LAMA", "matchs_nuls"]
    couleurs = ['y', 'b', 'r']

    plt.pie(yy, labels=Labels, colors=couleurs, autopct='%1.1f%%')
    plt.title("Répartition des résultats", fontsize=25, color="green")
    plt.legend()
    plt.show()
